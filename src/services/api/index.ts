import axios, {AxiosResponse} from 'axios'
import {GET_ANSWER} from '../../constants/endpints'

export interface IAnswer {
  answer: string
  forced: boolean
  image: string
}

interface INetworkService {
  getAnswer(): Promise<IAnswer | Error>
}

export class NetworkService implements INetworkService {
  async getAnswer(): Promise<IAnswer | Error> {
    try {
      const response: AxiosResponse = await axios.get(GET_ANSWER)
      const {data} = response
      return data
    } catch (e: any) {
      throw new Error(e)
    }
  }
}

const api = new NetworkService()
export default api
