import {useEffect, useState} from 'react'
import api from '../services/api'

interface IAnswerData {
  loading: boolean
  error: boolean
  errorMessage: string
  image: string | undefined
  answer: string
  forced: boolean
  request: () => void
}

export const useAnswer = (): IAnswerData => {
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<boolean>(false)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [errorMessage, setErrorMessage] = useState<string>('')
  const [image, setImage] = useState<string | undefined>(undefined)
  const [answer, setAnswer] = useState<string>('')
  const [forced, setForced] = useState<boolean>(false)

  useEffect(() => {
    request()
  }, [])

  const request = async () => {
    setLoading(true)
    try {
      // FIXME: type
      const data: any = await api.getAnswer()
      setLoading(false)
      setError(false)
      setImage(data.image)
      setAnswer(data.answer)
      setForced(data.forced)
    } catch (e) {
      console.log(e)
      setLoading(false)
      setError(true)
    }
  }

  return {
    loading,
    error,
    errorMessage,
    image,
    answer,
    forced,
    request,
  }
}
