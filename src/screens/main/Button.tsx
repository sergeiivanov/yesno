import React from 'react'
import {View, StyleSheet, TouchableOpacity, Image} from 'react-native'

interface IButtonProps {
  type: string
  onPress: () => void
  disabled: boolean
}

const Button: React.FC<IButtonProps> = (props: IButtonProps) => {
  return (
    // FIXME: inilte styles
    <View
      style={[
        styles.container,
        props.type === 'refresh' ? {left: 20} : {right: 20},
      ]}>
      <TouchableOpacity disabled={props.disabled} onPress={props.onPress}>
        <Image
          style={styles.image}
          source={{uri: props.type === 'refresh' ? 'refresh' : 'share'}}
        />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    zIndex: 3,
    position: 'absolute',
    bottom: 60,
  },
  image: {
    width: 40,
    height: 40,
    tintColor: 'white',
  },
})

export default Button
