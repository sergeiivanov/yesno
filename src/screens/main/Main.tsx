import React, {useState} from 'react'
import {View, StyleSheet, Share} from 'react-native'
import {Pulse} from 'react-native-animated-spinkit'
import FastImage from 'react-native-fast-image'
import {useAnswer} from '../../hooks/useAnswer'
import Answer from './Answer'
import Button from './Button'

const Main = () => {
  const {image, loading, answer, request} = useAnswer()
  const [imageLoaded, setImageLoaded] = useState<boolean>(false)

  const onShare = async () => {
    try {
      Share.share({
        message: answer,
        url: image,
      })
      // FIXME: type
    } catch (error: any) {
      console.log(error.message)
    }
  }

  return (
    <View style={styles.container}>
      <FastImage
        onLoadStart={() => setImageLoaded(false)}
        onLoadEnd={() => setImageLoaded(true)}
        style={styles.image}
        source={{
          uri: image,
          priority: FastImage.priority.high,
        }}
        resizeMode={FastImage.resizeMode.cover}
      />
      {imageLoaded ? (
        <Answer answer={answer} />
      ) : (
        <View style={styles.loading}>
          <Pulse size={150} color="#FFF" />
        </View>
      )}
      <Button disabled={loading} type="refresh" onPress={request} />
      <Button disabled={loading} type="share" onPress={onShare} />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#111111',
  },
  loading: {
    zIndex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: '100%',
  },
})

export default Main
