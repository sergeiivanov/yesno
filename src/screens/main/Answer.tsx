import React from 'react'
import {View, StyleSheet, Text} from 'react-native'

interface IAnswerProps {
  answer: string
}

const Answer: React.FC<IAnswerProps> = (props: IAnswerProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{props.answer}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    zIndex: 2,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 140,
    fontWeight: '900',
    color: 'white',
    textTransform: 'uppercase',
    textAlign: 'center',
  },
})

export default Answer
