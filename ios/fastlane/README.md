fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios buildAppProductionMethodAppStore
```
fastlane ios buildAppProductionMethodAppStore
```
Define info account without 2FA can upload build to TestFlight

Archive build production with method app store
### ios uploadToTestFlight
```
fastlane ios uploadToTestFlight
```
Upload build production to Test Flight
### ios uploadIPA
```
fastlane ios uploadIPA
```
Push a new build

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
